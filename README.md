# fun-cpu-limit

[![Coverage Status](https://coveralls.io/repos/gitlab/ochaton/fun-cpu-limit/badge.svg?branch=ci)](https://coveralls.io/gitlab/ochaton/fun-cpu-limit)

The `fun-cpu-limit` module is an extension for the `luafun` library, designed specifically to work with [Tarantool](https://www.tarantool.io/). This module provides a new method `cpu_limit` that helps to control the CPU consumption of non-yieldable fibers by forcing them to sleep after they have exhausted a specified time quota. This can be particularly useful in environments where CPU time needs to be carefully managed.

## Features

- **CPU Consumption Control**: Limit the CPU usage of your fibers in Tarantool.
- **Transaction Handling**: Customize behavior before yielding to manage transaction states effectively.

## Installation

To install `fun-cpu-limit`, you can use `luarocks`, `tarantoolctl rocks`, or `tt rocks` from the provided rocks server:

```bash
luarocks install fun-cpu-limit --server=https://rocks.ochaton.me.hb.vkcs.cloud/
tarantoolctl rocks install fun-cpu-limit --server=https://rocks.ochaton.me.hb.vkcs.cloud/
tt rocks install fun-cpu-limit --server=https://rocks.ochaton.me.hb.vkcs.cloud/
```

Alternatively, you can clone the repository from GitLab:

```bash
git clone https://gitlab.com/ochaton/fun-cpu-limit
```

Ensure that you have Tarantool and the `luafun` library installed before using this module.

## Usage

The `cpu_limit` method can be used with different configurations based on your needs regarding transaction management. Here's how to use it:

- **Commit Transaction**:

  ```lua
  :cpu_limit({ cpu_limit = 40, txn = 'commit' })
  ```

  This configuration commits any open transaction before yielding and starts a new one if the previous was closed by it.

- **Raise Exception**:

  ```lua
  :cpu_limit({ cpu_limit = 40, txn = 'raise' })
  ```

  Raises an exception if there is a transaction open before the yield, stopping the iteration.

- **Rollback Transaction**:

  ```lua
  :cpu_limit({ cpu_limit = 40, txn = 'rollback' })
  ```

  Rolls back any open transaction before yielding.

- **Default Behavior**:
  Without specifying a transaction behavior, if a transaction is open, Tarantool itself will raise an exception due to a yield: "Transaction has been aborted by a fiber yield".

## Example

Here is a simple example of how to use the `cpu_limit` method in your Lua scripts:

```lua
local fun = require('fun')  -- Make sure luafun is installed
require('fun-cpu-limit')    -- Load the fun-cpu-limit extension

-- Example usage
fun.range(1, 1000000):map(tostring):cpu_limit({cpu_limit = 50, txn = 'commit'}):length()
```

## Contributing

Contributions to `fun-cpu-limit` are welcome. Please feel free to fork the repository, make your changes, and submit a merge request on GitLab.

## License

`fun-cpu-limit` is available under the [MIT License](https://opensource.org/licenses/MIT). Feel free to use it in both personal and commercial projects.

For more details on usage and contributions, please visit the [project repository on GitLab](https://gitlab.com/ochaton/fun-cpu-limit).
