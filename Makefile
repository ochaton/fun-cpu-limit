.rocks/bin/luatest:
	tt rocks test --prepare

.rocks/bin/luacheck:
	tt rocks test --prepare

.rocks/bin/luacov-console:
	tt rocks test --prepare

run-tests: .rocks/bin/luatest
	.rocks/bin/luatest --coverage -c -v

run-linter: .rocks/bin/luacheck
	.rocks/bin/luacheck .

run-console-coverage: .rocks/bin/luacov-console
	.rocks/bin/luacov-console $$(pwd)
	.rocks/bin/luacov-console -s

test: run-linter run-tests run-console-coverage

