rockspec_format = "3.0"
package = "fun-cpu-limit"
version = "dev-1"
source = {
   url = "git+https://gitlab.com/ochaton/fun-cpu-limit",
}
description = {
   summary = "cpu_limit method for luafun",
   detailed = "cpu_limit method for luafun (Tarantool only)",
   homepage = "https://gitlab.com/ochaton/fun-cpu-limit",
   license = "MIT"
}
dependencies = {
   "tarantool"
}
build = {
   type = "builtin",
   modules = {
      ["fun.cpu_limit"] = "fun/cpu_limit.lua",
   }
}
test_dependencies = {
   "luatest",
   "luacov",
   "luacov-console",
   "luacov-coveralls",
   "luacheck"
}
test = {
   type = "command",
   command = "make test",
}
