local t = require 'luatest' --[[@as luatest]]
local basic = t.group('basic')
local fun = require 'fun.cpu_limit'

function basic.test_sum_100k()
	local sum = 0
	for _, num in fun.take_n(1e4, fun.cpu_limit(99, fun.ones())) do
		sum = sum + num
	end

	t.assert_is(sum, 1e4, "sum(take(1e4, ones())) == 1e4")
end

function basic.test_kv_map()
	local len = fun.range(1, 1e4)
		:map(function(no) return no, true end)
		:cpu_limit(99)
		:filter(function(_, x) return x == true end)
		:length()

	t.assert_is(len, 1e4, "cpu_limit does not truncate result values")
end
