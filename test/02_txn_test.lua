local t = require 'luatest' --[[@as luatest]]
local fiber = require 'fiber'
local txn = t.group('txn')
local fun = require 'fun.cpu_limit'

txn.before_all(function()
	box.cfg{}
	box.schema.space.create('test', { if_not_exists = true })
	box.space.test:format({
		{ name = 'id',    type = 'unsigned' },
		{ name = 'ctime', type = 'number' },
	})

	box.space.test:create_index('primary', { parts = {'id'}, if_not_exists = true })
	box.space.test:create_index('time', { parts = {'ctime', 'id'}, if_not_exists = true })
end)

function txn.test_space_creation()
	box.space.test:truncate()
	box.begin()
		local start = fiber.time()
		local len = fun.range(1, 2e4)
			:cpu_limit({ cpu_limit = 50, txn = 'commit' })
			:filter(function(no) return not box.space.test:get(no) end)
			:map(function(no) return box.space.test:insert({no, fiber.time()}) end)
			:length()
		local finish = fiber.time()
	box.commit()

	t.assert_is(len, 2e4, "2e4 tuples should be inserted")
	t.assert_is(box.space.test:len(), 2e4, "space len is 2e4")
	t.assert_lt(start, finish, "start < finish")
end


function txn.test_space_with_aborted_by_yield()
	box.space.test:truncate()
	t.assert_error(function()
		box.begin()
			fun.range(1, 1e5)
				:cpu_limit({ cpu_limit = 50 })
				:filter(function(no) return not box.space.test:get(no) end)
				:map(function(no) return box.space.test:insert({no, fiber.time()}) end)
				:length()
		box.commit()
	end, "Transaction has been aborted by a fiber yield")
end




